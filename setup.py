#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import setuptools
from setuptools import setup, find_packages


packagename = "code_report_tool"  # this has to be renamed

# consider the path of `setup.py` as root directory:
PROJECTROOT = os.path.dirname(sys.argv[0]) or "."
release_path = os.path.join(PROJECTROOT, "src", packagename, "release.py")
with open(release_path, encoding="utf8") as release_file:
    __version__ = release_file.read().split('__version__ = "', 1)[1].split('"', 1)[0]

with open("requirements.txt") as requirements_file:
    requirements = requirements_file.read()

with open("README.md") as readme_file:
    long_description=readme_file.read()

setup(
    name=packagename,
    version=__version__,
    author="CSDUMMI",
    author_email="csdummi.misquality@simplelogin.co",
    packages=find_packages("src"),
    package_dir={"": "src"},
    url="https://codeberg.org/developers/code-report-tool",
    license="AGPLv3",
    description="Turn your activity on a source forge into a report for resumes, websites, boasting and whatever it might be needed for.",
    long_description=long_description,
    install_requires=requirements,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: Affero GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python :: 3",
    ],
    entry_points={"console_scripts": [f"{packagename}={packagename}.script:main"]},
)
